
const axios = require('axios');

exports.postMessages = async (req, res) => {
  const chatInput = {
    message: req.body.message, // Ambil pesan dari body permintaan
  };

  try {
    const response = await axios.post('https://nusabasa-chatbot-n6iri666wa-et.a.run.app/chatbot/', chatInput);
    const chatOutput = response.data;

    res.status(200).json({ response: chatOutput.response });
  } catch (error) {
    console.error('Error:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};