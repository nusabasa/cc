const Material = require('../models/material');
const sequelize = require('../config/db');

exports.createMaterial = async (req, res) => {
    const {lessons, levelId, categoryId} = req.body;
    try {
        const material = await Material.create({
        CategoryId : categoryId,
        LevelId : levelId,
        lessons,
        });
        res.status(201).json(material);
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
}

exports.getMaterials = async (req, res) => {
    try {
        const materials = await Material.findAll();
        res.json(materials);
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: 'Failed to fetch materials' });
    }
}


exports.getMaterialbyParams = async (req, res) => {

    level = req.query.level;
    category = req.query.category;
    console.log(level, category)
    try {
        //getmaterial by level and category
        const materials = await Material.findAll({
            where: {
                LevelId: level,
                CategoryId: category
            }
        })
        res.json(materials);
    }catch(error){
        console.log(error);
        res.status(500).json({ error: 'Failed to fetch materials' });
    }

}


