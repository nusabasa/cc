const Level = require('../models/level');


exports.getLevels = async (req, res) => {
    try {
        const levels = await Level.findAll();
        res.json(levels);
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: 'Failed to fetch levels' });
    }
}

exports.createLevel = async (req,res)=>{
    const levelName = req.body.levelName;
    try {
        const level = await Level.create({
            name: levelName,
        })
        res.json(level)
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: 'Failed to create Level' });
    }
}