const {QuizMc,QuizFib,QuizTF} = require('../models/quiz');
const sequelize = require('../config/db');
const { updateUserpoint } = require('./userController');

// Get quiz based on the type
exports.getQuizMc = async (req, res) => {
    try {
        const quizMc = await QuizMc.findAll();
        res.json(quizMc);
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: 'Failed to fetch quizzes' });
    }
}
exports.getQuizTF = async (req, res) => {
    try {
        const quizTF = await QuizTF.findAll();
        res.json(quizTF);
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: 'Failed to fetch quizzes' });
    }
}
exports.getQuizFib = async (req, res) => {
    try {
        const quizFib = await QuizFib.findAll();
        res.json(quizFib);
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: 'Failed to fetch quizzes' });
    }
}

// Get quiz with specified category and level, expect LevelId and CategoryId
exports.getquizzesbyCategoryAndLevel = async (req, res)=>{
    const {category, level} = req.params;

    try {
        // Get 3 multiple choice quizzes
        const quizzesMc = await QuizMc.findAll({
            where: {
                LevelId: level,
                CategoryId: category
            },
            limit: 3,
            order: sequelize.random()
            
        });
        // Get one random true false quiz
        const quizzesTF = await QuizTF.findOne({
            where: {
                LevelId: level,
                CategoryId: category
            },
            order: sequelize.random()
        });
        
        // Get one random fill in blank quiz
        const quizzesFib = await QuizFib.findOne({
            where: {
                LevelId: level,
                CategoryId: category
            },
            order: sequelize.random()
        });

        // get the quizzes on specific order
        const quizzes = [quizzesMc[0],quizzesTF,quizzesMc[1],quizzesFib,quizzesMc[2]];
        
        res.json(quizzes)
        
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: 'Failed to fetch quizzes' });
    }

}
// Submit quiz result and update the userpoint
exports.submitQuiz = async (req,res)=>{
    const userId = req.userId;
    const point = req.body.point;

    // Update the user point
    const status = await updateUserpoint(userId,point);

    if(status==="error"){
        res.status(500).json({error: 'failed to submit quiz'})
    } else{
        res.status(200).json({success: 'quiz submited successfuly, point has been updated'})
    }
}


// Create Quizzes with bulk create, espect list of quiz as body
exports.bulkCreateQuizMc = async (req, res) => {
    
    const listJson = req.body.listQuiz;

    try {
        const quizzesMc = await QuizMc.bulkCreate(listJson)
        .then(() => console.log("Quiz created successfuly"));

        res.json({success: "Quiz created successfuly", quizzes: quizzesMc})
        
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: 'Failed to create quiz' })
    }
}
exports.bulkCreateQuizTF = async (req, res) => {

    const listJson = req.body.listQuiz;


    try {
        const quizzesTF = await QuizTF.bulkCreate(listJson)
        .then(() => console.log("Quiz created successfuly"));

        res.json({success: "Quiz created successfuly", quizzes: quizzesTF})

    } catch (error) {
        console.log(error);
        res.status(500).json({ error: 'Failed to create quiz' })
    }
}
exports.bulkCreateQuizFib = async (req, res) => {
    const listJson = req.body.listQuiz;

    try {
        const quizzesFib = await QuizFib.bulkCreate(listJson)
        .then(() => console.log("Quiz created successfuly"));

        res.json({success: "Quiz created successfuly", quizzes: quizzesFib})

    } catch (error) {
        console.log(error);
        res.status(500).json({ error: 'Failed to create quiz' })
    }
}