const Category = require('../models/category');


exports.getCategories = async (req, res) => {
    try {
        const categories = await Category.findAll();
        res.json(categories);
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: 'Failed to fetch categories' });
    }
}

exports.createCategory = async (req,res)=>{
    const categoryName = req.body.categoryName;
    const categoryDesc = req.body.categoryDesc;
    try {
        const category = await Category.create({
            name: categoryName,
            description : categoryDesc
        })
        res.json(category)
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: 'Failed to create category' });
    }
}