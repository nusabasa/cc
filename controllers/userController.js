const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../models/user");
const sequelize = require('../config/db');

// Register a new user
exports.registerUser = async (req, res) => {
  const { username, password, email } = req.body;

  // Check if any required fields are empty or not provided
  if (!username || !password || !email) {
    return res.status(400).json({ error: "All fields are required" });
  }

  // Trim whitespace from username, password, and email
  const trimmedUsername = username.trim();
  const trimmedPassword = password.trim();
  const trimmedEmail = email.trim();

  // Check if any trimmed fields are empty after removing whitespace
  if (!trimmedUsername || !trimmedPassword || !trimmedEmail) {
    return res.status(400).json({ error: "All fields must be filled" });
  }

  // Validate email format using regex
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  if (!emailRegex.test(trimmedEmail)) {
    return res.status(400).json({ error: "Invalid email format" });
  }

  try {
    // Check if the username already exists
    const existingUser = await User.findOne({ where: { username } });
    if (existingUser) {
      return res.status(409).json({ error: "Username already exists" });
    }

    // Check if the email is already registered
    const existingEmail = await User.findOne({ where: { email } });
    if (existingEmail) {
      return res.status(409).json({ error: "Email already registered" });
    }

    // Hash the password
    const hashedPassword = await bcrypt.hash(password, 10);

    // Create a new user
    const user = await User.create({
      username: trimmedUsername,
      password: hashedPassword,
      email: trimmedEmail,
    });

    console.log(user);
    res.json({ message: "User registered successfully" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Failed to register user" });
  }
};

// Login and generate JWT token
exports.loginUser = async (req, res) => {
  const { username, email, password } = req.body;

  // Check if username or email or password is empty or not provided
  if ((!username && !email) || !password) {
    return res
      .status(400)
      .json({ error: "Username/email and password are required" });
  }

  // Trim whitespace from username/email and password
  const trimmedUsername = username ? username.trim() : null;
  const trimmedEmail = email ? email.trim() : null;
  const trimmedPassword = password.trim();

  // Check if trimmed username/email or password is empty after removing whitespace
  if ((!trimmedUsername && !trimmedEmail) || !trimmedPassword) {
    return res
      .status(400)
      .json({ error: "Username/email and password cannot be empty" });
  }

  try {
    let user;

    // Check if the user exists using username or email
    if (trimmedUsername) {
      user = await User.findOne({ where: { username: trimmedUsername } });
    } else {
      user = await User.findOne({ where: { email: trimmedEmail } });
    }

    if (!user) {
      return res
        .status(401)
        .json({
          error: "Authentication failed. Invalid username/email or password.",
        });
    }

    // Compare passwords
    const passwordMatch = await bcrypt.compare(trimmedPassword, user.password);
    if (!passwordMatch) {
      return res
        .status(401)
        .json({
          error: "Authentication failed. Invalid username/email or password.",
        });
    }

    // Generate JWT token
    const token = jwt.sign({ userId: user.id }, "your-secret-key", {
      expiresIn: "7d",
    });
    res.json({
      token,
      userId: user.id,
      email : user.email,
      username: user.username,
      message: "Authentication successful",

    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Authentication failed" });
  }
};

exports.getUser = async (req, res) => {
  try {
    const user = await User.findAll();
    res.json(user);
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Failed to get user" });
  }
};


exports.updateUserpoint = async(userId, point) =>{
  try {
    const user = await User.findByPk(userId);

    // update the point
    const oldpoint = user.userpoint;
    user.userpoint = oldpoint + point;
    user.save()

    return "success";

  } catch (error) {
    return "error";
  }
}



exports.getUserById = async (req, res) => {
  try {
    const user = await User.findOne({ where: { id: req.params.id } });
    res.json(user);
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Failed to get user" });
  }
};


exports.getTopUsers = async (req, res) => {
  try {
    const topUsers = await User.findAll({
      attributes: ['username', 'userpoint'], // Only select 'username' and 'userpoint' columns
      order: [['userpoint', 'DESC']],
      limit: 10,
    });

    const usersArray = topUsers.map(user => ({
      username: user.username,
      userpoint: user.userpoint
    }));

    res.json(usersArray);
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Failed to get top users" });
  }
};
