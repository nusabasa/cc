  # API Documentation

This documentation provides information about the API endpoints available in your application.

# Link

URL : https://nusabasa-api-n6iri666wa-et.a.run.app


## Users

### Register a new user

- URL: `/users/register`
- Method: `POST`
- Description: Registers a new user.
- Request Body:
  - `username` (string, required): The username of the user.
  - `password` (string, required): The password of the user.
  - `email` (string, required): The email of the user.

### Login and generate JWT token

- URL: `/users/login`
- Method: `POST`
- Description: Logs in the user and generates a JWT token.
- Request Body:
  - `username` or `email` (string, required): The username/email of the user.
  - `password` (string, required): The password of the user.

### Get all users

- URL: `/users/get`
- Method: `GET`
- Description: Retrieves all users.
- Authorization: Requires a valid JWT token.

### Get top 10 leaderboard

- URL: `/users/leaderboard`
- Method: `GET`
- Description: Retrieves the top 10 users based on their userpoint.
- Authorization: Requires a valid JWT token.

## Chatbot

- URL: `/chatbot`
- Method: `POST`
- Description: This endpoint allows users to interact with the chatbot by sending a message and receiving a response. The message should be provided in the request body. The endpoint requires a valid JWT token for authorization. The chatbot uses natural language processing to understand the message and generate an appropriate response.
- Authorization : Requires a valid JWT token
- Request Body:
  - `message` (string, required): The message sent to the chatbot for processing and generating a response.

## Quiz  

### Get All Categories 

- URL: `/categories`
- Method: `GET`
- Description: Retrieves all categories.
- Authorization: unauthorize
```
[
    {
        "id": 1,
        "name": "percakapan",
        "description": "Belajar percakapan bareng nusabasa yuk!",
        "createdAt": "2023-06-13T07:55:41.499Z",
        "updatedAt": "2023-06-13T07:55:41.499Z"
    },
    {
        "id": 2,
        "name": "kata kerja",
        "description": "Belajar kata kerja bareng nusabasa yuk!",
        "createdAt": "2023-06-13T07:55:53.420Z",
        "updatedAt": "2023-06-13T07:55:53.420Z"
    },
    {
        "id": 3,
        "name": "kata benda",
        "description": "Belajar kata benda bareng nusabasa yuk!",
        "createdAt": "2023-06-13T07:56:00.532Z",
        "updatedAt": "2023-06-13T07:56:00.532Z"
    }
]
```

### Get All Levels

- URL: `/levels`
- Method: `GET`
- Description: Retrieves all levels.
- Authorization: unauthorize
```
[
    {
        "id": 1,
        "name": "mudah",
        "createdAt": "2023-06-13T07:56:13.450Z",
        "updatedAt": "2023-06-13T07:56:13.450Z"
    },
    {
        "id": 2,
        "name": "sedang",
        "createdAt": "2023-06-13T07:56:21.212Z",
        "updatedAt": "2023-06-13T07:56:21.212Z"
    },
    {
        "id": 3,
        "name": "sulit",
        "createdAt": "2023-06-13T07:56:28.383Z",
        "updatedAt": "2023-06-13T07:56:28.383Z"
    }
]
```

### Get All quiz

- URL: 
  - `/quiz/getMc` for multiple choices
  - `/quiz/getTF` for True False
  - `/quiz/getFib` for Fill in blank
- Method: `GET`
- Description: Retrieves all quizzes by the types.
- Authorization: Requires a valid JWT token.


### Get a Set of Specified Quizzes
- URL: `quiz/{categoryId}/{levelId}`
- Method: `GET`
- Description: get a set quiz of 5 based on the category and level
  - requires:
    - valid JWT token
    - categoryId and levelId as request params
  - return:
    - JSON of the quiz consist of 5 random question
    - There are three types of question: MC (multiple chocie), TF(true or false), FIB(fill in blank)
    - return example: of `GET` request on `quiz/1/1`  
    ```
    [
    {
        "id": 12,
        "a": "b",
        "b": "b",
        "c": "c",
        "d": "d",
        "question": "MC2",
        "answer": "a",
        "type": "MC",
        "createdAt": "2023-06-13T07:56:41.887Z",
        "updatedAt": "2023-06-13T07:56:41.887Z",
        "LevelId": 1,
        "CategoryId": 1
    },
    {
        "id": 3,
        "question": "truefalse3",
        "answer": true,
        "type": "TF",
        "createdAt": "2023-06-13T07:58:44.579Z",
        "updatedAt": "2023-06-13T07:58:44.579Z",
        "LevelId": 1,
        "CategoryId": 1
    },
    {
        "id": 11,
        "a": "b",
        "b": "b",
        "c": "c",
        "d": "d",
        "question": "MC1",
        "answer": "a",
        "type": "MC",
        "createdAt": "2023-06-13T07:56:41.887Z",
        "updatedAt": "2023-06-13T07:56:41.887Z",
        "LevelId": 1,
        "CategoryId": 1
    },
    {
        "id": 1,
        "question": "fib1",
        "answer": "ans",
        "type": "FIB",
        "createdAt": "2023-06-13T08:02:51.853Z",
        "updatedAt": "2023-06-13T08:02:51.853Z",
        "LevelId": 1,
        "CategoryId": 1
    },
    {
        "id": 15,
        "a": "b",
        "b": "b",
        "c": "c",
        "d": "d",
        "question": "MC5",
        "answer": "a",
        "type": "MC",
        "createdAt": "2023-06-13T07:56:41.887Z",
        "updatedAt": "2023-06-13T07:56:41.887Z",
        "LevelId": 1,
        "CategoryId": 1
    }
    ]
    ```
### Bulk Create for Quizzes

1. Multiple choice Quiz
  - URL: `/quiz/createBulkMc`
  - Method: `POST`
  - Request body:
  ```
  {
    "listQuiz": [
        {"question": "MC1", "answer":"a" ,"a":"b","b":"b","c":"c","d":"d", "LevelId":1, "CategoryId": 1},
        {"question": "MC2", "answer":"a" ,"a":"b","b":"b","c":"c","d":"d", "LevelId":1, "CategoryId": 1},
        {"question": "MC3", "answer":"a" ,"a":"b","b":"b","c":"c","d":"d", "LevelId":1, "CategoryId": 1},
        {"question": "MC4", "answer":"a" ,"a":"b","b":"b","c":"c","d":"d", "LevelId":1, "CategoryId": 1},
        {"question": "MC5", "answer":"a" ,"a":"b","b":"b","c":"c","d":"d", "LevelId":1, "CategoryId": 1}
    ]
  }
  ```
2. True False Quiz
  - URL: `/quiz/createBulkTF`
  - Method: `POST`
  - Request body:
  ```
  {
    "listQuiz": [
        {"question": "truefalse1", "answer": true, "LevelId":1, "CategoryId": 1},
        {"question": "truefalse2", "answer": true, "LevelId":1, "CategoryId": 1},
        {"question": "truefalse3", "answer": true, "LevelId":1, "CategoryId": 1}
    ]
  }
  ```
3. Fill in blank Quiz
  - URL: `/quiz/createBulkFib`
  - Method: `POST`
  - Request body:
  ```
  {
    "listQuiz": [
        {"question": "fib1", "answer":"ans", "LevelId":1, "CategoryId": 1},
        {"question": "fib2", "answer":"ans", "LevelId":1, "CategoryId": 1 },
        {"question": "fib3", "answer":"ans", "LevelId":1, "CategoryId": 1 }
    ]
  }
  ```

  If succeed all the request will return the following response:
  ```
  {
    "success": "Quiz created successfuly"
}
```


## Submit a quiz

- URL: /submitQuiz
- Method: POST
- Description: Submits a quiz and updates the user's point.
- Authorization: Requires a valid JWT token.
- Request Body:
  - `point` (number, required): The point obtained from the quiz.
  - `userId` (string, required): The userId of the user.

## Material

### Notes 

- CategoryId
  - Kata Sifat = 1
  - Kata Kerja = 2
  - Percakapan = 3

- LevelId
  - Easy = 1
  - Medium = 2
  - Hard = 3

### Get Specified Material

- URL : `/material/get?level={levelId}&category={categoryId}`
- URL Example : `https://nusabasa-api-n6iri666wa-et.a.run.app/material/get?level=1&category=1`
  - Method : `GET`
  - requires:
    - categoryId and levelId as request params
  - return example: of `GET` request on `material/get?level=1&category=1` 
     ```
      [
    {
        "id": 1,
        "lessons": [
            {
                "id": 1,
                "title": "Mengenal Kata Sifat Dalam Bahasa Jawa",
                "content": [
                    "Kata sifat dalam Bahasa Jawa dapat ditempatkan sebelum atau setelah kata benda yang dijelaskan, memberikan fleksibilitas dalam struktur kalimat. Misalnya, \"buku enak\" dan \"enak buku\" memiliki makna yang sama.",
                    "Kata sifat dalam Bahasa Jawa memiliki bentuk positif dan komparatif. Bentuk komparatif digunakan untuk membandingkan dua hal, sedangkan bentuk positif menggambarkan sifat secara langsung. Misalnya, \"enak\" (enak) adalah bentuk positif, sedangkan \"lebih enak\" adalah bentuk komparatif (lebih enak)."
                ]
            },
            {
                "id": 2,
                "title": "Contoh Ragam Kata Sifat",
                "content": [
                    "Cilik (Kecil) >< Gedhe (Besar)",
                    "Cedhak (Jauh) >< Adoh (Dekat)",
                    "Apik (Bagus) >< Elek(Jelek)",
                    "Dawa (Panjang) >< Cekak (Pendek)",
                    "Panas (Panas) >< Adhem (Dingin)",
                    "Cahya (Terang) >< Peteng (Gelap)",
                    "Larang (Mahal) >< Regane (Murah)",
                    "Kandel (Tebal) >< Kurus/Tipis (Tipis)",
                    "Dhuwur (Tinggi) >< Cendek (Rendah)",
                    "Legi (Manis) >< Pait (Pahit)"
                ]
            },
            {
                "id": 3,
                "title": "Penerapan Kata Sifat",
                "content": [
                    "Adhiku cilik banget (Adik saya kecil sekali)",
                    "Kampungku cedhak karo kutha (Desaku jauh dari kota)",
                    "Rumah iki apik banget (Rumah ini sangat bagus)",
                    "Ropeku dawa kok cekak (Tali panjang tapi pendek)",
                    "Akeh wong sing panas, kowe adhem (Banyak orang yang panas, kamu dingin)",
                    "Isine wong iki cahya bgt (Wajah orang ini sangat terang)",
                    "Keripiku regane larang (Keripiknya harganya mahal)",
                    "Buku iki kandel ben (Buku ini tebal banget)",
                    "Ora ana bocah sing cendek kaya kowe (Tidak ada anak yang rendah seperti kamu)",
                    "Kopi iki legi rasane (Kopi ini rasanya manis)"
                ]
            }
        ],
        "createdAt": "2023-06-15T11:31:07.879Z",
        "updatedAt": "2023-06-15T11:31:07.879Z",
        "LevelId": 1,
        "CategoryId": 1
    }
        ]
    ```

