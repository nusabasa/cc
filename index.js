const express = require('express');
const bodyParser = require('body-parser');
const userRoutes = require('./router/userRoutes');
const categoryRoutes = require('./router/categoryRoutes');
const levelRoutes = require('./router/levelRoutes')
const quizRoutes = require('./router/quizRoutes')
const chatbotRoutes = require('./router/chatbotRoutes')
const materialRoutes = require('./router/materialRoutes')
const sequelize  = require('./config/db');
const cors = require('cors');

const app = express();
app.use(bodyParser.json());

// Enable CORS for all routes
app.use(cors());

// Mount the user routes
app.use('/users', userRoutes);
// Mount the category routes
app.use('/categories', categoryRoutes);
// Mount the level routes
app.use('/levels',levelRoutes)
// Mount the quiz routes
app.use('/quiz',quizRoutes)

app.use('/chatbot',chatbotRoutes )
app.use('/material',materialRoutes)


app.get('/', (req, res) => {
   res.send('Welcome To Nusabasa API'); 
})

// Menghubungkan ke basis data
sequelize
  .sync()
  .then(() => {
    console.log('Database connection has been established successfully.');
  })
  .catch((error) => {
    console.error('Unable to connect to the database:', error);
  });


const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
