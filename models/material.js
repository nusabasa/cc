const { DataTypes } = require("sequelize");
const sequelize = require("../config/db");
const Level = require("./level");
const Category = require("./category");

const Material = sequelize.define("Material", {
  lessons: {
    type: DataTypes.ARRAY(DataTypes.JSON),
    allowNull: false,
  },
});

Material.belongsTo(Level);
Material.belongsTo(Category);

module.exports = Material;
