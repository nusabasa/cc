// File: models/Level.js
const { DataTypes } = require('sequelize');
const sequelize = require('../config/db');
const Category = require('./category');

const Level = sequelize.define('Level', {
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
});

module.exports = Level;
