// File: models/Quiz.js
const { DataTypes } = require('sequelize');
const sequelize = require('../config/db');
const Level = require('./level');
const Category = require('./category');

const QuizMc = sequelize.define('QuizMc', {
  a: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  b: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  c: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  d: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  question: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  answer: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  type: {
    type: DataTypes.STRING,
    defaultValue: "MC",
  }
});

QuizMc.belongsTo(Level);
QuizMc.belongsTo(Category);

const QuizTF = sequelize.define('QuizTF',{
  question: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  answer: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
  },
  type: {
    type: DataTypes.STRING,
    defaultValue: "TF",
  }
});

QuizTF.belongsTo(Level);
QuizTF.belongsTo(Category);

const QuizFib = sequelize.define('QuizFib',{
  question: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  answer: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  type: {
    type: DataTypes.STRING,
    defaultValue: "FIB",
  }
});

QuizFib.belongsTo(Level);
QuizFib.belongsTo(Category);

module.exports = {QuizMc,QuizFib,QuizTF};
