const express = require('express');
const chatbotController = require('../controllers/chatbotController');
const { authorizeToken } = require('../middleware/middlewares');
const router = express.Router();


router.post('/',authorizeToken, chatbotController.postMessages)

module.exports = router;