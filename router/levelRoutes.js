const express = require('express');
const router = express.Router();
const levelController = require('../controllers/levelController');
const {authorizeToken} = require("../middleware/middlewares");

router.get('/' ,levelController.getLevels);
router.post('/create',levelController.createLevel);

module.exports = router;