const express = require('express');
const router = express.Router();
const quizController = require('../controllers/quizController');
const {authorizeToken} = require("../middleware/middlewares");

router.get('/getMc' ,quizController.getQuizMc);
router.get('/getTF' ,quizController.getQuizTF);
router.get('/getFib' ,quizController.getQuizFib);

router.get('/:category/:level',authorizeToken, quizController.getquizzesbyCategoryAndLevel)

router.post('/createBulkMc',quizController.bulkCreateQuizMc)
router.post('/createBulkTF',quizController.bulkCreateQuizTF)
router.post('/createBulkFib',quizController.bulkCreateQuizFib)

router.post('/submitQuiz',authorizeToken,quizController.submitQuiz)

module.exports = router;