const express = require('express');
const router = express.Router();
const materialController = require('../controllers/materialController');

router.post('/create',materialController.createMaterial);
router.get('/',materialController.getMaterials);
router.get('/get/', materialController.getMaterialbyParams);



module.exports = router;